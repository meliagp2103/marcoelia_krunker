﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour,IDamageable
{
    //needs to be changed by a fcking scriptable
    public PlayerData data;

    private int refHealth;

    public int RefHealth { get { return refHealth; } }

    public Camera mainCamera;

    public CinemachineVirtualCamera fpsCam;

    [HideInInspector]
    public Rigidbody rb;

    public Weapon currentWeapon;

    public WeaponPivotBehaviour weaponPivot;

    // Start is called before the first frame update
    void Awake()
    {
        data = Instantiate(data);

        rb = GetComponent<Rigidbody>();

        refHealth = data.health;
    }

    private void Start()
    {
        fpsCam.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(GameManager.Instance.currentGameState == GameState.GamePlay)
        {
            if (refHealth > 0)
            {
                if (!fpsCam.enabled)
                {
                    fpsCam.enabled = true;
                }

                int previousWeapon = weaponPivot.selectedWeapon;

                transform.rotation = new Quaternion(transform.rotation.x, mainCamera.transform.rotation.y, transform.rotation.z, transform.rotation.w);

                if (InputHandler.GetKey(typeof(MoveForwardCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(MoveForwardCommand),this);
                }

                if (InputHandler.GetKey(typeof(MoveRightCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(MoveRightCommand), this);
                }

                if (InputHandler.GetKey(typeof(MoveBackCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(MoveBackCommand), this);
                }

                if (InputHandler.GetKey(typeof(MoveLeftCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(MoveLeftCommand), this);
                }

                if (InputHandler.GetKeyDown(typeof(JumpCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(JumpCommand), this);
                }

                if (Input.GetMouseButton(0))
                {
                    InputHandler.ExecuteCommand(typeof(ShootCommand), currentWeapon);
                }

                if (Input.GetMouseButtonDown(1))
                {
                    InputHandler.ExecuteCommand(typeof(AimCommand),(currentWeapon,true));
                }

                if (Input.GetMouseButtonUp(1))
                {
                    InputHandler.ExecuteCommand(typeof(AimCommand), (currentWeapon,false));
                }

                if (InputHandler.GetKeyDown(typeof(CrouchCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(CrouchCommand), (transform,new Vector3(transform.localScale.x, transform.localScale.y / 2, transform.localScale.z)));
                }

                if (InputHandler.GetKeyUp(typeof(CrouchCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(CrouchCommand), (transform, new Vector3(transform.localScale.x, transform.localScale.y * 2, transform.localScale.z)));
                }

                if (InputHandler.GetKeyDown(typeof(ReloadCommand)))
                {
                    InputHandler.ExecuteCommand(typeof(ReloadCommand), currentWeapon);
                }

                if (Input.GetAxis("Mouse ScrollWheel") > 0f && currentWeapon.IsAiming == false)
                {
                    if (weaponPivot.selectedWeapon >= weaponPivot.transform.childCount - 1)
                    {
                        weaponPivot.selectedWeapon = 0;
                    }

                    else
                    {
                        weaponPivot.selectedWeapon++;
                    }
                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0f && currentWeapon.IsAiming == false)
                {
                    if (weaponPivot.selectedWeapon <= 0)
                    {
                        weaponPivot.selectedWeapon = weaponPivot.transform.childCount - 1;
                    }

                    else
                    {
                        weaponPivot.selectedWeapon--;
                    }
                }

                if (previousWeapon != weaponPivot.selectedWeapon)
                {
                    InputHandler.ExecuteCommand(typeof(SwitchCommand), weaponPivot);

                    currentWeapon = weaponPivot.transform.GetChild(weaponPivot.selectedWeapon).GetComponent<Weapon>();
                }
            }

            else
            {
                GameManager.Instance.ChangeState(GameState.EndGame);

                GameManager.Instance.UnlockMouse();
            }

            if(transform.position.y <= 0)
            {
                GameManager.Instance.ChangeState(GameState.EndGame);

                GameManager.Instance.UnlockMouse();
            }
        }

        else
        {
            if (fpsCam.enabled)
            {
                fpsCam.enabled = false;
            }
        }
    }

    public void TakeDamage(int damageAmount)
    {
       
        if(refHealth > 0)
        {
            refHealth -= damageAmount;
        }

        else
        {
            gameObject.SetActive(false);
            //GameManager.Instance.ChangeState(GameState.EndGame);
        }

    }

}
