﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public  abstract class Weapon : MonoBehaviour
{
    //ScriptableObject!!!!!!

    public WeaponData data;

    //public BulletBehaviour bullet;

    protected int refClipAmount;

    public int RefClipAmount { get { return refClipAmount; } }

    protected int refMaxAmmoCapacity;

    public int ReFMaxAmmoCapacity { get { return refMaxAmmoCapacity; } }

    protected float lastTimeShooted;

    protected bool isReloading;

    protected bool isAiming;

    public bool IsAiming { get { return isAiming; } }

    protected float defaultZoomValue;

    private Animator animator;

    public Transform bulletSpawn;

    public CinemachineVirtualCamera FpsCam;

    public Camera playerCam;


    protected void OnEnable()
    {
        isReloading = false;
    }

    protected void Awake()
    {
        data = Instantiate(data);

        animator = GetComponent<Animator>();
    }


    // Start is called before the first frame update
    protected virtual void Start()
    {
        refClipAmount = data.clipAmount;

        refMaxAmmoCapacity = data.maxAmmoCapacity;

        defaultZoomValue = FpsCam.m_Lens.FieldOfView;

    }

    //// Update is called once per frame
    //protected virtual void Update()
    //{
       
    //}

    public virtual void Shoot()
    {
        if (Time.time > lastTimeShooted + data.rateOfFire && refClipAmount > 0)
        {
            lastTimeShooted = Time.time;

            refClipAmount--;

            //Debug.Log("bullets remaining: " + refClipAmount);

            GameObject bullet = ObjectPool.Instance.GetPooledObject("Bullet");

            if (bullet != null)
            {

                var bulletComponent = bullet.GetComponent<BulletBehaviour>();

                float x = Random.Range(- data.spreadX, data.spreadX);

                float y = Random.Range(- data.spreadY, data.spreadY);

                //Debug.Log($"{bulletSpawn.name} {bulletSpawn.transform.position}");

                bullet.transform.position = bulletSpawn.transform.position;

                bullet.transform.rotation = bulletSpawn.transform.rotation;

                bulletComponent.direction = bulletSpawn.forward + new Vector3(x, y, 0);

                bulletComponent.damage = data.damage;

                //Debug.Log("Bullet placed,rotated and setted correctly");

                bullet.gameObject.SetActive(true);

            }
        }
    }

    public virtual IEnumerator Reload()
    {
        if (!isReloading && refClipAmount < data.clipAmount && refMaxAmmoCapacity > 0)
        {
            Debug.Log("Reloading....");

            isReloading = true;

            yield return new WaitForSeconds(data.reloadTime);

            refClipAmount = data.clipAmount;

            Debug.Log("clip succesfully swapped");

            refMaxAmmoCapacity -= data.clipAmount;

            isReloading = false;

            Debug.Log("decreasing ammo");
        }
    }

    public virtual void Aim(bool aim)
    {
        isAiming = aim;

        if (isAiming)
        {
            animator.SetTrigger("isAiming");
        }

        else
        {
            animator.SetTrigger("reverseAiming");
        }
    }

    public void ZoomIn()
    {
        FpsCam.m_Lens.FieldOfView = data.zoomValue;
    }

    public void ZoomOut()
    {
        FpsCam.m_Lens.FieldOfView = defaultZoomValue;
    }
}
