﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public enum GameState
{
    MainMenu,
    GamePlay,
    Pause,
    EndGame
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    
    //placeHolder
    public GameState currentGameState;

    public event Action<GameState> showCurrentPage;

    [HideInInspector]
    public int enemyCount;

    [HideInInspector]
    public bool victory = false;

    //[SerializeField]
    //private GameObject optionPage;

    [HideInInspector]
    public CommandsReceiver commandReceiver;

    private EnemyBehaviour[] enemycountList;

    private void Awake()
    {
        if(!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        enemycountList = FindObjectsOfType<EnemyBehaviour>();

        commandReceiver = GetComponent<CommandsReceiver>();

        enemyCount = enemycountList.Length;

        Debug.Log(enemyCount);

        InputHandler.FirstScheme = UIManager.Instance.optionsPage.GetComponentsInChildren<InputKeyMapper>().ToList();
    }

    // Start is called before the first frame update
    private void Start()
    {
        currentGameState = GameState.MainMenu;

        showCurrentPage?.Invoke(currentGameState);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && currentGameState == GameState.GamePlay)
        {
            ChangeState(GameState.Pause);

            UnlockMouse();
        }

        if(enemyCount <= 0)
        {
            victory = true;

            ChangeState(GameState.EndGame);

            UnlockMouse();
        }

        //checking winning condition
    }


    public void ChangeState(GameState nextGameState)
    {
        currentGameState = nextGameState;

        showCurrentPage?.Invoke(nextGameState);
    }

    public void LockMouse()
    {
        Cursor.visible = false;

        Cursor.lockState = CursorLockMode.Locked;
    }

    public void UnlockMouse()
    {
        Cursor.visible = true;

        Cursor.lockState = CursorLockMode.None;
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
