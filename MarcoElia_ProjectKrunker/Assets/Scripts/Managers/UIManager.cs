﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public static UIManager Instance;

    public GameObject mainMenuPage;

    public GameObject gamePlayPage;

    public GameObject pausePage;

    public GameObject optionsPage;

    public GameObject victoryPage;

    public GameObject gameOverPage;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }

        else
        {
            Destroy(this);
        }
    }

    //// Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.showCurrentPage += OnShowCurrentPage;
    }

    //// Update is called once per frame
    //void Update()
    //{

    //}

    public void OnShowCurrentPage(GameState state)
    {
        switch (state)
        {
            case GameState.MainMenu:
                {
                    mainMenuPage.SetActive(true);
                    gamePlayPage.SetActive(false);
                    pausePage.SetActive(false);
                    optionsPage.SetActive(false);
                    victoryPage.SetActive(false);
                    gameOverPage.SetActive(false);
                    break;
                }

            case GameState.GamePlay:
                {
                    mainMenuPage.SetActive(false);
                    gamePlayPage.SetActive(true);
                    pausePage.SetActive(false);
                    optionsPage.SetActive(false);
                    victoryPage.SetActive(false);
                    gameOverPage.SetActive(false);
                    break;
                }

            case GameState.Pause:
                {
                    mainMenuPage.SetActive(false);
                    gamePlayPage.SetActive(false);
                    pausePage.SetActive(true);
                    optionsPage.SetActive(false);
                    victoryPage.SetActive(false);
                    gameOverPage.SetActive(false);
                    break;
                }

            case GameState.EndGame:
                {

                    if (GameManager.Instance.victory)
                    {
                        mainMenuPage.SetActive(false);
                        gamePlayPage.SetActive(false);
                        pausePage.SetActive(false);
                        optionsPage.SetActive(false);
                        victoryPage.SetActive(true);
                        gameOverPage.SetActive(false);
                    }

                    else
                    {
                        mainMenuPage.SetActive(false);
                        gamePlayPage.SetActive(false);
                        pausePage.SetActive(false);
                        optionsPage.SetActive(false);
                        victoryPage.SetActive(false);
                        gameOverPage.SetActive(true);
                    }
                   
                    break;
                }
        }
    }
}
