﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CommandsReceiver : MonoBehaviour
{

    public void MoveForward(object data)
    {
        PlayerBehaviour player = (PlayerBehaviour)data;

        player.rb.MovePosition(player.rb.transform.position + player.transform.forward * player.data.speed * Time.fixedDeltaTime);
    }

    public void MoveLeft(object data)
    {
        PlayerBehaviour player = (PlayerBehaviour)data;

        player.rb.MovePosition(player.rb.transform.position - player.transform.right * player.data.speed * Time.fixedDeltaTime);
    }

    public void MoveRight(object data)
    {
        PlayerBehaviour player = (PlayerBehaviour)data;

        player.rb.MovePosition(player.rb.transform.position + player.transform.right * player.data.speed * Time.fixedDeltaTime);
    }

    public void MoveBack(object data)
    {
        PlayerBehaviour player = (PlayerBehaviour)data;

        player.rb.MovePosition(player.rb.transform.position - player.transform.forward * player.data.speed * Time.fixedDeltaTime);
    }

    public void Shoot(object data)
    {
        Weapon weapon = (Weapon)data;

        weapon.Shoot();
    }

    public void Reload(object data)
    {
        Weapon weapon = (Weapon)data;

        weapon.StartCoroutine("Reload");
    }

    public void Aim(object data)
    {
        Weapon weapon = ((ValueTuple<Weapon,bool>)data).Item1;

        var aim = ((ValueTuple<Weapon, bool>)data).Item2;

        weapon.Aim(aim);
    }

    public void Crouch(object data)
    {
        Transform player = ((ValueTuple<Transform,Vector3>)data).Item1;

        Vector3 targetScale = ((ValueTuple<Transform, Vector3>)data).Item2;

        player.localScale = targetScale;
    }


    public void Switch(object data)
    {
        WeaponPivotBehaviour weaponPivot = (WeaponPivotBehaviour)data;

        weaponPivot.Switch();

    }

    public void Jump(object data)
    {
        PlayerBehaviour player = (PlayerBehaviour)data;

        if (Physics.Raycast(player.transform.position, - player.transform.up, 1.1f))
        {
            player.rb.AddForce(player.transform.up * player.data.jumpSpeed, ForceMode.Impulse);
        }
    }
}
