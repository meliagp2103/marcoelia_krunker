﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.Switch(data);
}
