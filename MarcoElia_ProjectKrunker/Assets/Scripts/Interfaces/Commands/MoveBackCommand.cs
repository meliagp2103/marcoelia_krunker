﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.MoveBack(data);
}
