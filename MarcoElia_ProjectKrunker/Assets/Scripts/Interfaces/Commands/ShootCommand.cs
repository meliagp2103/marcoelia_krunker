﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.Shoot(data);
}
