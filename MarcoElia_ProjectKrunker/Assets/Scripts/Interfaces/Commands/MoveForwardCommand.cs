﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForwardCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.MoveForward(data);
}
