﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRightCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.MoveRight(data);
   
}
