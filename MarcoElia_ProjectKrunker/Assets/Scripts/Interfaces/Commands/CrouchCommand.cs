﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.Crouch(data);
}
