﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.Aim(data);
}
