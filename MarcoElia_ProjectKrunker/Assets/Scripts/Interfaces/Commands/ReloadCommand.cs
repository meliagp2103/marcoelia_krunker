﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.Reload(data);
}
