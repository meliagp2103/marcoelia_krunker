﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeftCommand : ICommand
{
    public void Execute(CommandsReceiver receiver, object data) => receiver.MoveLeft(data);
}
