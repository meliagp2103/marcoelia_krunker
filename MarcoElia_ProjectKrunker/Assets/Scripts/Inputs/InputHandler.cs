﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct InputHandler
{
    private static Dictionary<ICommand, InputKeyMapper> bindings = new Dictionary<ICommand, InputKeyMapper>();

    private static List<InputKeyMapper> firstScheme;

    public static List<InputKeyMapper> FirstScheme
    {
        get => firstScheme;
        set
        {
            firstScheme = value;
            ResetCommands(firstScheme);
        }
    }

    public enum Command
    {
        AimCommand,
        CrouchCommand,
        JumpCommand,
        MoveBackCommand,
        MoveForwardCommand,
        MoveLeftCommand,
        MoveRightCommand,
        NothingCommand,
        ReloadCommand,
        ShootCommand,
        SwitchCommand,
    }

    public static void ResetCommands(List<InputKeyMapper> scheme)
    {
        bindings.Clear();
        scheme.ForEach(x => SetCommand(x));
    }

    public static void ExecuteCommand(Type command, object data = null) => GetCommand(command).Execute(GameManager.Instance.commandReceiver,data);

    public static ICommand GetCommand(Type command) => (ICommand)Activator.CreateInstance(command);

    public static void SetCommand(InputKeyMapper field)
    {
        //starting from the generic "Type" search if you can find a class with the same Name of an element of the commands enum
       Type command = Type.GetType(Enum.GetName(typeof(Command), field.command));

        //looks how many associations of the same type are inside the bindings
       int currentBindings = bindings.Count(x => x.Key.GetType().Equals(command));

        //if the number corrispond to 0 (it means is the first association it will be added)
        if(currentBindings == 0)
        {
            bindings.Add(GetCommand(command), field);
        }

        //else removes the last one
        else
        {
            bindings.Where(x => x.Value != field && x.Value.Key == field.Key).ToList().ForEach(x => x.Value.Key = KeyCode.None);
        }
    }

    //override of the GetKey functions
    public static bool GetKey(Type command) => Input.GetKey(bindings.First(x => x.Key.GetType().Equals(command)).Value.Key);

    public static bool GetKeyDown(Type command) => Input.GetKeyDown(bindings.First(x => x.Key.GetType().Equals(command)).Value.Key);

    public static bool GetKeyUp(Type command) => Input.GetKeyUp(bindings.First(x => x.Key.GetType().Equals(command)).Value.Key);

    public static KeyCode KeyBound(Type command) => bindings.First(x => x.Key.GetType().Equals(command)).Value.Key;

}

