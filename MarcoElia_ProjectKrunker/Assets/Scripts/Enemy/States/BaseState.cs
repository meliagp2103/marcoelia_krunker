﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseState : StateMachineBehaviour
{

    public  EnemyBehaviour enemy;

    public PlayerBehaviour player;

   

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<EnemyBehaviour>();

        player = animator.gameObject.GetComponent<EnemyBehaviour>().player;
    }
}
