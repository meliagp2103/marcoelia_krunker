﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadState : BaseState
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        if (GameManager.Instance.currentGameState == GameState.GamePlay)
        {
            GameManager.Instance.enemyCount--;

            enemy.gameObject.SetActive(false);
        }

        //maybe invoke the method that decrease the enemy counter
    }
}
