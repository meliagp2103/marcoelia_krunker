﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class PatrolState : BaseState
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(GameManager.Instance.currentGameState == GameState.GamePlay)
        {
            if ((enemy.agent.remainingDistance <= enemy.agent.stoppingDistance && !enemy.agent.pathPending) || enemy.currentPathStatus == NavMeshPathStatus.PathInvalid)
            {
                enemy.SetNextDestination(true);
            }

            else if (enemy.agent.isPathStale)
            {
                enemy.SetNextDestination(false);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //maybe insert stuff here pls
    }
}
