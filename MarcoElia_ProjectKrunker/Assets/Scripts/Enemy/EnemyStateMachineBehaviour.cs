﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public abstract class EnemyStateMachineBehaviour : MonoBehaviour,IDamageable
{
    public EnemyData data;

    [HideInInspector]
    public NavMeshAgent agent;

    public Animator animator;

    public PlayerBehaviour player;

    public AIPath path;

    public GameObject weapon;

    [HideInInspector]
    public NavMeshPathStatus currentPathStatus = NavMeshPathStatus.PathComplete; 

    public GameObject weaponSpawnPoint;

    [HideInInspector]
    public int refHealth;

    protected int currentIndex;

    protected float lastTimeAttacked;

    [HideInInspector]
    public float refClipAmount;

    protected float lastTimeReloaded;

    protected bool isReloading;

    protected void Awake()
    {
        Instantiate(data);

        refHealth = data.health;

        refClipAmount = data.clipAmount;

        agent = GetComponent<NavMeshAgent>();

        animator = GetComponent<Animator>();
    }


    // Start is called before the first frame update
    protected void Start()
    {
        
    }

    // Update is called once per frame
    protected void Update()
    {
        animator.SetFloat("distance", Mathf.Abs(Vector3.Distance(player.gameObject.transform.position, transform.position)));

        animator.SetInteger("health", refHealth);
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(transform.position, 40f);

    //    Gizmos.DrawWireSphere(transform.position, 20f);
    //}

    public void SetNextDestination(bool isIncrementing)
    {
        //checking if the path exists or not
        if (!path) return;

        //this variable checks if we are changing the target position
        // 1 = we are changing the target so this will become the "Path Increaser"
        // 0 = the target is the same and this variable will beacame a "Path Resetter"
        int incStep = isIncrementing ? 1 : 0;

        //if the current index + incStep is greater than the path Lenght,resets the path to the first waypoint,else return the next index
        int nextWaypointIndex = (currentIndex + incStep >= path.waypoints.Count) ? 0 : currentIndex + incStep;

        //Fetching the waypoint
        Transform nextWaypointTransform = path.waypoints[nextWaypointIndex];

        //setting the destination
        if (nextWaypointTransform != null)
        {
            currentIndex = nextWaypointIndex;

            agent.destination = nextWaypointTransform.position;

            return;
        }

    }

    public void Attack()
    {
        if(Time.time > lastTimeAttacked + data.rateOfFire)
        {
            lastTimeAttacked = Time.time;

            refClipAmount--;

            //Debug.Log(refClipAmount);

            GameObject bullet = ObjectPool.Instance.GetPooledObject("Bullet");

            if (bullet != null)
            {
                var bulletComponent = bullet.GetComponent<BulletBehaviour>();

                float x = Random.Range(-data.spreadX, data.spreadX);

                float y = Random.Range(-data.spreadY, data.spreadY);

                bullet.transform.position = weaponSpawnPoint.transform.position;

                //Debug.Log($"{weaponSpawnPoint.name} {weaponSpawnPoint.transform.position}");

                bullet.transform.rotation = weaponSpawnPoint.transform.rotation;

                bulletComponent.direction = transform.forward + new Vector3(x, y, 0);

                bulletComponent.damage = data.damage;

                bullet.gameObject.SetActive(true);

            }
        }
    }

    public IEnumerator Reload()
    {
        if (!isReloading && refClipAmount < data.clipAmount)
        {
            isReloading = true;

            yield return new WaitForSeconds(data.reloadTime);

            refClipAmount = data.clipAmount;

            Debug.Log("clip succesfully swapped");

            isReloading = false;

            Debug.Log("decreasing ammo");
        }
    }

    public void TakeDamage(int damageAmount)
    {
        if (refHealth > 0)
        {
            refHealth -= damageAmount;
        }
    }
}
