﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon Data", menuName = "Settings/Weapon Data", order = 2)]
public class WeaponData : ScriptableObject
{
    public int damage;

    public float reloadTime;

    public float rateOfFire;

    public float spreadX;

    public float spreadY;

    [Range(10, 60)]
    public float zoomValue = 10;

    public int clipAmount;

    public int maxAmmoCapacity;
}
