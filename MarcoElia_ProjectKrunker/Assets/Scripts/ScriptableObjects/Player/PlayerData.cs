﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Data",menuName ="Settings/Player Data",order = 0)]
public class PlayerData : ScriptableObject
{
    public float speed;

    public float jumpSpeed;

    public int health;
}
