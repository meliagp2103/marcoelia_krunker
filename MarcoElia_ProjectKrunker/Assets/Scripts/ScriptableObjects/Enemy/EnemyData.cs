﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Enemy Data", menuName = "Settings/Enemy Data", order = 1)]

public class EnemyData : ScriptableObject
{
    public int health;

    public int damage;

    public float rateOfFire;

    public float spreadX;

    public float spreadY;

    public float reloadTime;

    public int clipAmount;
}
