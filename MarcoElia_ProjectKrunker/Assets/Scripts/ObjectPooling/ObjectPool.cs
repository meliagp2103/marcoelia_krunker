﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem
{
    public GameObject objectToPool;

    public int amountToPool;

    public bool expand = true;
}


public class ObjectPool : MonoBehaviour
{

    public static ObjectPool Instance;

    public List<GameObject> pooledObjects;

    public List<ObjectPoolItem> itemsToPool;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        pooledObjects = new List<GameObject>();

        foreach(ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.amountToPool; i++)
            {
                GameObject obj = Instantiate(item.objectToPool);

                obj.SetActive(false);

                pooledObjects.Add(obj);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetPooledObject(string tag)
    {
        for(int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                //Debug.Log("Returned the bullet: " + i);
                return pooledObjects[i];
            }
        }

        foreach(ObjectPoolItem item in itemsToPool)
        {
            if(item.objectToPool.tag == tag)
            {
                if (item.expand)
                {
                    GameObject obj = Instantiate(item.objectToPool);

                    obj.SetActive(false);

                    pooledObjects.Add(obj);

                    return obj;
                }
            }
           
        }

        return null;
    }
}
