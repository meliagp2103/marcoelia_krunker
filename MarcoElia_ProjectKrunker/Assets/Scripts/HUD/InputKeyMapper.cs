﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputKeyMapper : MonoBehaviour,IPointerClickHandler,IDeselectHandler
{
    public InputHandler.Command command;

    [SerializeField]
    private KeyCode key;

    [HideInInspector]
    public TextMeshProUGUI input;

    public KeyCode Key
    {
        get => key;
        set
        {
            key = value;
            input.text = key.ToString();
        }
    }

    private void Awake() => input = GetComponent<TextMeshProUGUI>();


    private void Start() => input.text = key.ToString();

    
    private void OnGUI()
    {
        Event e = Event.current;

        if(e.isKey && input.text == "wait for input")
        {
            Key = e.keyCode;

            InputHandler.SetCommand(this);

            //after changed the key setting the gameObject to null
            EventSystem.current.SetSelectedGameObject(null);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if(input.text != "wait for input")
            {
                input.text = "wait for input";      
            }

            else
            {
                Key = key;
                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }

    public void OnDeselect(BaseEventData eventData)
    {
        if(input.text == "wait for input")
        {
            Key = key;
        }
    }
}
