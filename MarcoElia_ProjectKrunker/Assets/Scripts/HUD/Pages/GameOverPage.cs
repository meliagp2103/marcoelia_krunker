﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPage : MonoBehaviour
{
    public void RestartGame()
    {
        GameManager.Instance.Reset();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
