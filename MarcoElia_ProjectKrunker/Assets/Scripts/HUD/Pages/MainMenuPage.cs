﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuPage : MonoBehaviour
{
    public void StartGame()
    {
        GameManager.Instance.ChangeState(GameState.GamePlay);

        GameManager.Instance.LockMouse();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
