﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionPage : MonoBehaviour
{
    public Button backButton;


    // Update is called once per frame
    void Update()
    {
        if (enabled)
        {
            if(GameManager.Instance.currentGameState == GameState.MainMenu)
            {
               backButton.onClick.AddListener(EnableMenu);
            }

            if (GameManager.Instance.currentGameState == GameState.Pause)
            {
                backButton.onClick.AddListener(EnablePause); 
            }
        }
    }

    public void EnableMenu()
    {
        gameObject.SetActive(false);

        UIManager.Instance.mainMenuPage.SetActive(true);

    }

    public void EnablePause()
    {
        gameObject.SetActive(false);

        UIManager.Instance.pausePage.SetActive(true);
    }
}
