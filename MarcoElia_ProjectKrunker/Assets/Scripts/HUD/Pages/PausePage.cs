﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePage : MonoBehaviour
{
    public void ResumeGame()
    {
        GameManager.Instance.ChangeState(GameState.GamePlay);

        GameManager.Instance.LockMouse();
    }

    public void BackToMenu()
    {
        GameManager.Instance.ChangeState(GameState.MainMenu);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
