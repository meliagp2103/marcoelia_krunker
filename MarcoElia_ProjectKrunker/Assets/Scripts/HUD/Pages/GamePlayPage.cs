﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GamePlayPage : MonoBehaviour
{
    public Slider playerHealthBar;

    public TMP_Text clipText;

    public TMP_Text maxAmmoText;

    private PlayerBehaviour player;

    // Start is called before the first frame update
    void Awake()
    {
        player = FindObjectOfType<PlayerBehaviour>();

        playerHealthBar.maxValue = player.data.health;

        clipText.text = player.currentWeapon.data.clipAmount.ToString();

        maxAmmoText.text = player.currentWeapon.data.maxAmmoCapacity.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        playerHealthBar.value = player.RefHealth;

        clipText.text = player.currentWeapon.RefClipAmount.ToString();

        maxAmmoText.text = player.currentWeapon.ReFMaxAmmoCapacity.ToString();

    }

}
