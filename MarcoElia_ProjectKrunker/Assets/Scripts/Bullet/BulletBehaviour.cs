﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{

    [SerializeField]
    private float bulletSpeed;
    public float BulletSpeed { get { return bulletSpeed; } }

    [SerializeField]
    private float bulletLifeTime;
    public float BulletLifeTime { get { return bulletLifeTime; } }

    [HideInInspector]
    public Vector3 direction;

    [HideInInspector]
    public int damage;


    private Rigidbody rb;

    private float refBulletLifeTime;


    // Start is called before the first frame update
    void OnEnable()
    {
        refBulletLifeTime = bulletLifeTime;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (refBulletLifeTime >= 0)
        {
            refBulletLifeTime -= Time.deltaTime;
        }

        else
        {
            gameObject.SetActive(false);

            refBulletLifeTime = bulletLifeTime;
        }
    }

    private void FixedUpdate()
    {
        Move(direction);
    }

    void Move(Vector3 direction)
    {
        rb.velocity = direction * bulletSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        IDamageable damageable = other.GetComponent<IDamageable>();

        Debug.Log(other.gameObject.name);

        if (damageable != null)
        {
            damageable.TakeDamage(damage);

            Debug.Log("Damaged: " + other.gameObject.name + " Inflicted: " + damage);

        }

        gameObject.SetActive(false);
    }
}
