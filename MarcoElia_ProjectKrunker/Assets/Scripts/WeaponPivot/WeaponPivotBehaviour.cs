﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPivotBehaviour : MonoBehaviour
{

    public int selectedWeapon = 0;

    public void Switch()
    {
        int i = 0;

        foreach(Transform weapon in transform)
        {
            if(i == selectedWeapon)
            {
                weapon.gameObject.SetActive(true);
            }

            else
            {
                weapon.gameObject.SetActive(false);
            }

            i++;
        }
    }
}
